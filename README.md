I file necessari per eseguire la build del sistema devono essere contenuti all'interno di una sottodirectory nella root del proprio progetto, in modo da poter utilizzare questa repository come modulo di git. Il flusso di creazione di un nuovo progetto, dunque, consiste in:
1. Creare in locale la directory per il nuovo progetto, ad esempio:
```
$ mkdir test
```
2. Inizializzare la repository Git:
```
$ git init
```
3. Installare il modulo Git di questa repository (verrà creato in una nuova sottodirectory `build`:
```
$ git submodule add git@bitbucket.org:michelezamuner/exercise-phing.git build
```
4. Configurare il progetto modificando opportunamente il file `project.conf`.
5. Eseguire l'inizializzazione del progetto invocando `phing` nella directory `build` con target `init`:
```
$ cd build
$ phing init
```
6. Per poter fare il deploy, è necessario avere un accesso al server di destinazione che non richieda l'inserimento di password. Affinché questo sia possibile, accertarsi di aver aggiunto la propria chiave pubblica alla lista di chiavi autorizzate (`authorized_keys`) dell'utente del server a cui ci si connette. Accertarsi, inoltre, che questo utente abbia accesso in scrittura al percorso nel quale si vogliono inserire i file (perché ovviamente sarà questo utente, lato server, a fare la scrittura).

L'inizializzazione del progetto consiste nel creare il file `.gitignore`, e le directory `.ide`, `dev` e `src` (in futuro potrebbero essere previste altre directory, come una per i test).

La directory `dev` contiene di default un file `changelog` e un primo dump del database (un file `.sql` il cui nome dipende dalla configurazione del progetto). La directory `.ide` è esclusa dal controllo di git, ed è pensata per inserirvi i file usati dal proprio IDE, ad esempio NetBeans. La directory `src` contiene i sorgenti del progetto, e saranno gli unici ad essere copiati nel server durante il deploy.

Il task di commit aggiunge automaticamente una riga al file `changelog`, segnalando il momento in cui è stato fatto il commit. Per la sintassi del file `changelog` fare riferimento alla relativa documentazione su [wiki](http://en.wikipedia.org/wiki/Changelog) e su [GNU](http://www.gnu.org/prep/standards/html_node/Style-of-Change-Logs.html).

## TODO LIST
* Controllare che tutti i task usati nel build abbiano degli opportuni controlli di situazioni eccezionali, e terminino con opportuni messaggi d'errore.
* Modificare lo script in modo che capisca se alcune tabelle presenti nel vecchio database remoto sono state eliminate da quello locale (ad esempio, plugin eliminati), perché facendo semplicemente un dump del database locale e importandolo in remoto vengono sì aggiunte eventuali tabelle mancanti, ma non vengono eliminate eventuali tabelle vecchie.
* Lo script andrà probabilmente modificato quando si comincerà a lavorare con `composer`. Non è detto che sarà necessario includere i moduli con l'opzione `submodule` di git, perché uno potrebbe non essere interessato a modificare
il modulo.
